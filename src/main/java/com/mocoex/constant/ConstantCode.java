package com.mocoex.constant;

public class ConstantCode {
	
	
	public static final String C_GENERAL_SUCCESS_CODE              		   		= "000";
	public static final String C_GENERAL_ERROR_CODE              		   		= "999";
	
	public static final String C_GENERAL_SUCCESS_MESSAGE              		   	= "Success";
	public static final String C_GENERAL_ERROR_MESSAGE              		   	= "General Error";
	
	//MONEY MANAGEMENT
	public static final String C_MONEY_MANAGEMENT_ERROR_DATA_NULL_CODE          = "900";
	public static final String C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_CODE     = "901";
	public static final String C_MONEY_MANAGEMENT_INVALID_INSERT_DATA_CODE      = "902";
	public static final String C_MONEY_MANAGEMENT_WARNING_INSERT_DATA_CODE      = "080";
	public static final String C_MONEY_MANAGEMENT_FAILED_INSERT_DATA_CODE      	= "981";
	
	public static final String C_MONEY_MANAGEMENT_ERROR_DATA_NULL_MESSAGE       = "Data cannot be null";
	public static final String C_MONEY_MANAGEMENT_ERROR_CLIENT_ID_NULL_MESSAGE  = "Client id cannot be null";
	public static final String C_MONEY_MANAGEMENT_INVALID_INSERT_DATA_MESSAGE   = "Invalid data insert";
	public static final String C_MONEY_MANAGEMENT_WARNING_INSERT_DATA_MESSAGE   = "Warning, Insert data is not going well";
	public static final String C_MONEY_MANAGEMENT_FAILED_INSERT_DATA_MESSAGE   	= "Failed insert data";

}
