package com.mocoex.model.moneyManager;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReqInsertIncomeModel {
	
	@JsonProperty("client_id")
	private String clientId;

	@JsonProperty("income_data")
	private List<IncomeModel> incomeData;

	public List<IncomeModel> getIncomeData() {
		return incomeData;
	}

	public void setIncomeData(List<IncomeModel> incomeData) {
		this.incomeData = incomeData;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
}
