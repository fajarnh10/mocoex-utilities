package com.mocoex.model.moneyManager;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReqInsertExpenseModel {
	
	@JsonProperty("client_id")
	private String clientId;
	
	@JsonProperty("expense_data")
	private List<ExpenseModel> expenseData;

	public List<ExpenseModel> getExpenseData() {
		return expenseData;
	}

	public void setExpenseData(List<ExpenseModel> expenseData) {
		this.expenseData = expenseData;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
}
